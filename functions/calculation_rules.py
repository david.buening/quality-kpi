"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")

def kpi_mass(system: LegoAssembly)->float:
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_mass (float): Sum of masses of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass()")

    total_mass = 0
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
    return total_mass # alternative: sum(c.properties["mass [g]"] for c in system.get_component_list(-1))

# Add new functions for calculating metrics

#Gesamt Kosten
def kpi_cost(system: LegoAssembly)->float:
    total_cost=sum(c.properties["price [Euro]"] for c in system.get_component_list(-1))
    return total_cost

#Maximale Lieferzeit
def kpi_max_delivery_time(system: LegoAssembly)->float:
    max_delivery_time = 0
    for c in system.get_component_list(-1):
        max_delivery_time = max([max_delivery_time,c.properties["delivery time [days]"]])
    return max_delivery_time

#Gesamtanzahl Teile
def kpi_component_number(system: LegoAssembly)->float:
    component_number = len(system.get_component_list(-1))
    return component_number
    
    
    
    
